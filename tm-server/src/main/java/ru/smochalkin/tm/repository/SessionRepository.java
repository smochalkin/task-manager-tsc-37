package ru.smochalkin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.ISessionRepository;
import ru.smochalkin.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private final String table = "tm_session";

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return table;
    }

    @Override
    @SneakyThrows
    public @NotNull Session fetch(@NotNull ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setUserId(row.getString("user_id"));
        session.setTimestamp(row.getTimestamp("time_stamp").getNanos());
        session.setSignature(row.getString("signature"));
        return session;
    }

    @Override
    @SneakyThrows
    public void add(@NotNull Session session) {
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                "(id, user_id, signature, time_stamp) VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getSignature());
        statement.setTimestamp(4, new Timestamp(session.getTimestamp()));
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @SneakyThrows
    public List<Session> findAllByUserId(@Nullable String userId) {
        @NotNull final List<Session> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) result.add(fetch(resultSet));
                return result;
            }
        }
    }

    @Override
    @SneakyThrows
    public void removeByUserId(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

}
