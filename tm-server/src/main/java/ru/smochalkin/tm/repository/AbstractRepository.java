package ru.smochalkin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    protected abstract E fetch(@NotNull final ResultSet row);

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String query = "TRUNCATE TABLE " + getTableName();
        try (
                @NotNull final Statement statement = connection.createStatement()
        ) {
            statement.executeUpdate(query);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final List<E> result = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName();
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @Override
    @NotNull
    @SneakyThrows
    public E findById(@Nullable final String id) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                if (!resultSet.next()) throw new EntityNotFoundException();
                return fetch(resultSet);
            }
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public int getCount() {
        @NotNull final String query =
                "SELECT count(*) AS count FROM " + getTableName();
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(query)) {
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        try {
            findById(id);
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

}

