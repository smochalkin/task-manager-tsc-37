package ru.smochalkin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final String table = "tm_user";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return table;
    }

    @Override
    @SneakyThrows
    public @NotNull User fetch(@NotNull ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password_hash"));
        user.setFirstName(row.getString("first_name"));
        user.setLastName(row.getString("last_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setEmail(row.getString("email"));
        user.setLock(row.getBoolean("lock"));
        user.setRole(Role.getRole(row.getString("role")));
        return user;
    }

    @Override
    @SneakyThrows
    public final void add(@NotNull final String login, @NotNull final String password) {
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                "(id, login, password_hash, role) VALUES (?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, UUID.randomUUID().toString());
            statement.setString(2, login);
            statement.setString(3, password);
            statement.setString(4, Role.USER.name());
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public final void add(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                "(id, login, password_hash, email, role) VALUES (?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, UUID.randomUUID().toString());
            statement.setString(2, login);
            statement.setString(3, password);
            statement.setString(4, email);
            statement.setString(5, Role.USER.name());
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET first_name = ?, last_name = ?, middle_name = ?" +
                " WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, middleName);
            statement.setString(4, id);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void updatePasswordById(@NotNull final String id, @NotNull final String password) {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET password_hash = ?" +
                " WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, password);
            statement.setString(2, id);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void updateLockByLogin(@NotNull final String login, final boolean lock) {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET lock = ?" +
                " WHERE login = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setBoolean(1, lock);
            statement.setString(2, login);
            statement.executeUpdate();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE login = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                if (!resultSet.next()) throw new EntityNotFoundException();
                return fetch(resultSet);
            }
        }
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@NotNull final String login) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE login = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);
            statement.executeUpdate();
        }
    }

    @Override
    public Boolean isLogin(@NotNull final String login) {
        try {
            findByLogin(login);
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

}