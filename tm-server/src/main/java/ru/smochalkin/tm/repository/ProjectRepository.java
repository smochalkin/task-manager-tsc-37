package ru.smochalkin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    @NotNull
    private final String table = "tm_project";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return table;
    }

    @Override
    @SneakyThrows
    public @NotNull Project fetch(@NotNull ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setUserId(row.getString("user_id"));
        project.setStatus(Status.getStatus(row.getString("status")));
        project.setCreated(row.getTimestamp("created"));
        project.setStartDate(row.getTimestamp("start_date"));
        project.setEndDate(row.getTimestamp("end_date"));
        return project;
    }

    @Override
    @SneakyThrows
    public final void add(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final String description
    ) {
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                "(id, name, description, status, USER_ID) VALUES (?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, UUID.randomUUID().toString());
            statement.setString(2, name);
            statement.setString(3, description);
            statement.setString(4, Status.NOT_STARTED.name());
            statement.setString(5, userId);
            statement.executeUpdate();
        }
    }

}
