package ru.smochalkin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.AbstractBusinessEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    public AbstractBusinessRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<E> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<E> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) result.add(fetch(resultSet));
                return result;
            }
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId, @NotNull final String sort) {
        @NotNull final List<E> result = new ArrayList<>();
        @NotNull final String preparedSort = Sort.getSort(sort).name();
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE user_id = ? ORDER BY " + preparedSort;
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) result.add(fetch(resultSet));
                return result;
            }
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public E findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                if (!resultSet.next()) throw new EntityNotFoundException();
                return fetch(resultSet);
            }
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public E findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND name = ? LIMIT 1";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, name);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                if (!resultSet.next()) throw new EntityNotFoundException();
                return fetch(resultSet);
            }
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public E findByIndex(@NotNull final String userId, final int index) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? LIMIT 1 OFFSET ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                if (!resultSet.next()) throw new EntityNotFoundException();
                return fetch(resultSet);
            }
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND name = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, name);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String id = findByIndex(userId, index).getId();
        removeById(userId, id);
    }

    @Override
    @SneakyThrows
    public void updateById(@NotNull final String id, @NotNull final String name, @Nullable final String desc) {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET name = ?, description = ?" +
                " WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, name);
            statement.setString(2, desc);
            statement.setString(3, id);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @Nullable final String desc
    ) {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET name = ?, description = ?" +
                " WHERE user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, name);
            statement.setString(2, desc);
            statement.setString(3, userId);
            statement.setString(4, id);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @Nullable final String desc
    ) {
        @NotNull final String id = findByIndex(userId, index).getId();
        updateById(userId, id, name, desc);
    }

    @Override
    @SneakyThrows
    public void updateStatusById(
            @NotNull String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) {
        @NotNull String query = "UPDATE " + getTableName() +
                " SET status = ?, start_date = ?, end_date = ?" +
                " WHERE user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            prepareStatus(statement, status);
            statement.setString(4, userId);
            statement.setString(5, id);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) {
        @NotNull final String id = findByName(userId, name).getId();
        updateStatusById(userId, id, status);
    }

    @Override
    @SneakyThrows
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) {
        @NotNull final String id = findByIndex(userId, index).getId();
        updateStatusById(userId, id, status);
    }

    @Override
    @SneakyThrows
    public int getCountByUser(@NotNull final String userId) {
        @NotNull final String query = "SELECT count(*) AS count FROM " + getTableName() + " WHERE user_id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                return resultSet.getInt("count");
            }
        }
    }

    @SneakyThrows
    private void prepareStatus(@NotNull final PreparedStatement statement, @NotNull final Status status) {
        statement.setString(1, status.toString());
        switch (status) {
            case IN_PROGRESS:
                statement.setTimestamp(2, new Timestamp(new Date().getTime()));
                statement.setNull(3, 0);
                break;
            case COMPLETED:
                statement.setNull(2, 0);
                statement.setTimestamp(3, new Timestamp(new Date().getTime()));
                break;
            case NOT_STARTED:
                statement.setNull(2, 0);
                statement.setNull(3, 0);
        }
    }

}
