package ru.smochalkin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @NotNull
    private final String table = "tm_task";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return table;
    }

    @Override
    @SneakyThrows
    public @NotNull Task fetch(@NotNull ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setUserId(row.getString("user_id"));
        task.setStatus(Status.getStatus(row.getString("status")));
        task.setCreated(row.getTimestamp("created"));
        task.setStartDate(row.getTimestamp("start_date"));
        task.setEndDate(row.getTimestamp("end_date"));
        task.setProjectId(row.getString("project_id"));
        return task;
    }

    @Override
    @SneakyThrows
    public final void add(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final String description
    ) {
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                "(id, name, description, status, USER_ID) VALUES (?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, UUID.randomUUID().toString());
            statement.setString(2, name);
            statement.setString(3, description);
            statement.setString(4, Status.NOT_STARTED.name());
            statement.setString(5, userId);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void bindTaskById(@NotNull final String projectId, @NotNull final String taskId) {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = ?" +
                " WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, projectId);
            statement.setString(2, taskId);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void bindTaskById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = ?" +
                " WHERE user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, projectId);
            statement.setString(2, userId);
            statement.setString(3, taskId);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskById(@NotNull final String projectId, @NotNull final String taskId) {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = NULL" +
                " WHERE project_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, projectId);
            statement.setString(2, taskId);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskById(@NotNull final String userId, @NotNull final String projectId,  @NotNull final String taskId) {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = NULL" +
                " WHERE project_id = ? AND user_id = ? AND id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, projectId);
            statement.setString(2, userId);
            statement.setString(3, taskId);
            statement.executeUpdate();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Task> findTasksByProjectId(@NotNull final String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE project_id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, projectId);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) result.add(fetch(resultSet));
                return result;
            }
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Task> findTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND project_id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) result.add(fetch(resultSet));
                return result;
            }
        }
    }

    @Override
    @SneakyThrows
    public void removeTasksByProjectId(@NotNull final String projectId) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE project_id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, projectId);
            statement.executeUpdate();
        }
    }

}