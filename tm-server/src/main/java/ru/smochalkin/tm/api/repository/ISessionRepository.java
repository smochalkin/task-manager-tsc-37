package ru.smochalkin.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    void add(@NotNull Session session);

    @NotNull List<Session> findAllByUserId(@Nullable String userId);

    void removeByUserId(@NotNull String userId);

}
