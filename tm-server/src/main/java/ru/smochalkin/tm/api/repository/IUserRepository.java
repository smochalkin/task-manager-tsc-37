package ru.smochalkin.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    void add(@NotNull String login, @NotNull String password);

    void add(@NotNull String login, @NotNull String password, @NotNull String email);

    void updateById(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    void updatePasswordById(@NotNull String id, @NotNull String password);

    void updateLockByLogin(@NotNull String login, boolean lock);

    @NotNull
    User findByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

    Boolean isLogin(@NotNull String login);

}
