package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.exception.empty.*;
import ru.smochalkin.tm.exception.system.LoginExistsException;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.repository.UserRepository;
import ru.smochalkin.tm.util.HashUtil;

import java.sql.Connection;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull IPropertyService propertyService,
            @NotNull IConnectionService connectionService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    public IUserRepository getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isLogin(login)) throw new LoginExistsException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            try {
                repository.add(login, getPasswordHash(password));
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isLogin(login)) throw new LoginExistsException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            try {
                repository.add(login, getPasswordHash(password), email);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByLogin(login);
        }
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@NotNull final String login) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            try {
                repository.removeByLogin(login);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            try {
                repository.updatePasswordById(id, getPasswordHash(password));
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            try {
                repository.updateById(id, firstName, lastName, middleName);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.isLogin(login);
        }
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            try {
                repository.updateLockByLogin(login, true);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            try {
                repository.updateLockByLogin(login, false);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @NotNull
    private String getPasswordHash(@NotNull final String password) {
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        return HashUtil.salt(password, secret, iteration);
    }

}

