package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IProjectTaskService;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.repository.ProjectRepository;
import ru.smochalkin.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    public ITaskRepository getTaskRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    public IProjectRepository getProjectRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(@Nullable final String projectId, @Nullable final String taskId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = getTaskRepository(connection);
            try {
                repository.bindTaskById(projectId, taskId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = getTaskRepository(connection);
            try {
                repository.bindTaskById(userId, projectId, taskId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskByProjectId(@Nullable final String projectId, @Nullable final String taskId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = getTaskRepository(connection);
            try {
                repository.unbindTaskById(projectId, taskId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = getTaskRepository(connection);
            try {
                repository.unbindTaskById(userId, projectId, taskId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Task> findTasksByProjectId(@Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = getTaskRepository(connection);
            return repository.findTasksByProjectId(projectId);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Task> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = getTaskRepository(connection);
            return repository.findTasksByProjectId(userId, projectId);
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            try {
                taskRepository.removeTasksByProjectId(id);
                projectRepository.removeById(id);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            Project project = projectRepository.findByName(userId, name);
            removeProjectById(project.getId());
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (isEmpty(userId)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            Project project = projectRepository.findByIndex(userId, index);
            removeProjectById(project.getId());
        }
    }

}
