package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.exception.empty.EmptyLoginException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.exception.empty.EmptyPasswordException;
import ru.smochalkin.tm.exception.system.LoginExistsException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.repository.ProjectRepository;

import java.sql.Connection;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public IProjectRepository getRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @Override
    @SneakyThrows
    public void create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = getRepository(connection);
            try {
                repository.add(userId, name, description);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

}
