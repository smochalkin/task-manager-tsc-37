package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.api.repository.ISessionRepository;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.api.service.ISessionService;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyLoginException;
import ru.smochalkin.tm.exception.empty.EmptyPasswordException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.exception.system.UserIsLocked;
import ru.smochalkin.tm.model.Session;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.repository.SessionRepository;
import ru.smochalkin.tm.util.HashUtil;

import java.sql.Connection;
import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionService(
            @NotNull IConnectionService connectionService,
            @NotNull ServiceLocator serviceLocator
    ) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

    @Override
    @NotNull
    @SneakyThrows
    public final Session open(@NotNull final String login, @NotNull final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        if (user.isLock()) throw new UserIsLocked();
        @NotNull final String secret = serviceLocator.getPropertyService().getPasswordSecret();
        @NotNull final Integer iteration = serviceLocator.getPropertyService().getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(password, secret, iteration);
        if (!user.getPasswordHash().equals(hash)) throw new AccessDeniedException();
        @NotNull final Session session = new Session(user.getId());
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = getRepository(connection);
            try {
                repository.add(session);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
        return sign(session);
    }

    @Override
    @SneakyThrows
    public final void close(@NotNull final Session session) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = getRepository(connection);
            try {
                repository.removeById(session.getId());
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public final void closeAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = getRepository(connection);
            try {
                repository.removeByUserId(userId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public final void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionTarget = sign(temp);
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = getRepository(connection);
            if (!repository.existsById(session.getId())) throw new AccessDeniedException();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        final @NotNull User user = serviceLocator.getUserService().findById(session.getUserId());
        if (!user.getRole().equals(role)) throw new AccessDeniedException();
    }

    @NotNull
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final IPropertyService ps = serviceLocator.getPropertyService();
        @Nullable final String signature = HashUtil.salt(session, ps.getSignSecret(), ps.getSignIteration());
        session.setSignature(signature);
        return session;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Session> findAllByUserId(@Nullable final String userId) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = getRepository(connection);
            return repository.findAllByUserId(userId);
        }
    }

}