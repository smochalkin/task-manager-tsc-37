package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected abstract IRepository<E> getRepository(@NotNull final Connection connection);

    @Override
    @SneakyThrows
    public void clear() {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            try {
                repository.clear();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<E> findAll() {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public E findById(@Nullable final String id) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findById(id);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(final String id) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            try {
                repository.removeById(id);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public int getCount() {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.getCount();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

}
