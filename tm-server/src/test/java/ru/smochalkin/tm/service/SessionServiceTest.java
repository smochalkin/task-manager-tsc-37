package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.component.Bootstrap;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.model.Session;
import ru.smochalkin.tm.model.User;

import java.util.List;

public class SessionServiceTest {

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    private int sessionCount;

    @Before
    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        userService = new UserService(propertyService, connectionService);
        sessionService = new SessionService(connectionService, new Bootstrap());
        userService.create("test1", "1");
        userService.create("test2", "2");
        sessionService.open("test1","1");
        sessionCount = sessionService.getCount();
    }

    @After
    public void end() {
        @NotNull final User user1 = userService.findByLogin("test1");
        @NotNull final User user2 = userService.findByLogin("test1");
        userService.removeByLogin("test1");
        userService.removeByLogin("test2");
        sessionService.closeAllByUserId(user1.getId());
        sessionService.closeAllByUserId(user2.getId());
    }

    @Test
    public void openTest() {
        sessionService.open("test2", "2");
        Assert.assertEquals(sessionCount + 1, sessionService.getCount());
    }

    @Test
    public void closeTest() {
        @NotNull final User user = userService.findByLogin("test1");
        @NotNull final Session session = sessionService.findAllByUserId(user.getId()).get(0);
        sessionService.close(session);
        Assert.assertEquals(sessionCount - 1, sessionService.getCount());
    }

    @Test
    public void closeAllByUserIdTest() {
        @NotNull final String userId = userService.findByLogin("test1").getId();
        @NotNull final List<Session> sessions = sessionService.findAllByUserId(userId);
        @NotNull final int resultSize = sessionService.getCount() - sessions.size();
        sessionService.closeAllByUserId(userId);
        Assert.assertEquals(resultSize, sessionService.getCount());
    }

    @Test
    public void validateTest() {
        @NotNull final Session session = sessionService.open("test1", "1");
        session.setSignature(null);
        Assert.assertThrows(AccessDeniedException.class,
                () -> sessionService.validate(session));
    }

    @Test
    public void validateRoleTest() {
        @NotNull final Session session = sessionService.open("test1", "1");
        Assert.assertThrows(AccessDeniedException.class,
                () -> sessionService.validate(session, Role.ADMIN));
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final String userId = sessionService.findAll().get(0).getUserId();
        @NotNull final List<Session> sessions = sessionService.findAllByUserId(userId);
        Assert.assertEquals(userId, sessions.get(0).getUserId());
    }

}
