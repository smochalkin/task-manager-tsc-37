package ru.smochalkin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.service.ConnectionService;
import ru.smochalkin.tm.service.PropertyService;
import ru.smochalkin.tm.util.HashUtil;

import java.sql.Connection;
import java.util.List;

public class UserRepositoryTest {

    private static final int ENTRY_COUNT = 5;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private Connection connection;

    private int repositorySize;

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        connection = connectionService.getConnection();
        @NotNull IPropertyService propertyService = new PropertyService();
        userRepository = new UserRepository(connection);
        for (int i = 1; i <= ENTRY_COUNT; i++) {
            @NotNull final String passwordHash = HashUtil.salt(
                    i + "", propertyService.getPasswordSecret(), propertyService.getPasswordIteration()
            );
            try {
                userRepository.add("test" + i, passwordHash, i + "@test");
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        }
        repositorySize = userRepository.getCount();
    }

    @After
    @SneakyThrows
    public void end() {
        for (int i = 1; i <= ENTRY_COUNT; i++) {
            try {
                userRepository.removeByLogin("test" + i);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        }
    }

    @Test
    @SneakyThrows
    public void addTest() {
        userRepository.add("testAdd", "", "@test");
        connection.commit();
        @NotNull final User user = userRepository.findByLogin("testAdd");
        Assert.assertEquals("testAdd", user.getLogin());
    }

    @Test
    @SneakyThrows
    public void removeByLoginTest() {
        userRepository.removeByLogin("testAdd");
        connection.commit();
        Assert.assertThrows(EntityNotFoundException.class,
                () -> userRepository.findByLogin("testAdd"));
    }

    @Test
    public void findAllTest() {
        @NotNull final List<User> actualUserList = userRepository.findAll();
        Assert.assertEquals(repositorySize, actualUserList.size());
    }

    @Test
    public void findByLoginTest() {
        @NotNull final User userByLogin = userRepository.findByLogin("test1");
        Assert.assertEquals("test1", userByLogin.getLogin());
    }

    @Test
    public void findByIdTest() {
        @NotNull final User userByLogin = userRepository.findByLogin("test2");
        @NotNull final User userById = userRepository.findById(userByLogin.getId());
        Assert.assertEquals("test2", userById.getLogin());
    }

    @Test
    public void existsByIdTest() {
        @NotNull final User userByLogin = userRepository.findByLogin("test3");
        Assert.assertTrue(userRepository.existsById(userByLogin.getId()));
    }

    @Test
    public void getCountTest() {
        Assert.assertEquals(repositorySize, userRepository.getCount());
    }

    @Test
    public void isLoginTest() {
        Assert.assertTrue(userRepository.isLogin("test4"));
        Assert.assertFalse(userRepository.isLogin("not user"));
    }

}
