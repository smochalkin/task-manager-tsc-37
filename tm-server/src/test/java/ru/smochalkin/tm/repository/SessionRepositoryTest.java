package ru.smochalkin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.ISessionRepository;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.Session;
import ru.smochalkin.tm.service.ConnectionService;
import ru.smochalkin.tm.service.PropertyService;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionRepositoryTest {

    private static final int ENTRY_COUNT = 5;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private Connection connection;

    @NotNull
    private ISessionRepository sessionRepository;

    @NotNull
    private List<Session> sessionList;

    private int repositorySize;

    @Before
    public void init() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        connection = connectionService.getConnection();
        sessionList = new ArrayList<>();
        sessionRepository = new SessionRepository(connection);
        for (int i = 0; i < ENTRY_COUNT; i++) {
            @NotNull final Session session = new Session();
            if ((i < ENTRY_COUNT / 2))
                session.setUserId(USER_ID_1);
            else
                session.setUserId(USER_ID_2);
            sessionRepository.add(session);
            sessionList.add(session);
        }
        repositorySize = sessionRepository.getCount();
    }

    @After
    @SneakyThrows
    public void end() {
        for (@NotNull final Session session : sessionList) {
            try {
                sessionRepository.removeById(session.getId());
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        }
    }

    @Test
    public void addTest() {
        @NotNull final Session newSession = new Session();
        newSession.setUserId(USER_ID_1);
        sessionRepository.add(newSession);
        Assert.assertEquals(repositorySize + 1, sessionRepository.getCount());
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Session> actualSessionList = sessionRepository.findAll();
        Assert.assertEquals(repositorySize, actualSessionList.size());
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> sessionRepository.findById(UUID.randomUUID().toString()));
        for (@NotNull final Session session : sessionList) {
            Assert.assertEquals(session.getId(), sessionRepository.findById(session.getId()).getId());
        }
    }

    @Test
    public void existsByIdTest() {
        @NotNull final String isId = sessionList.get(0).getId();
        @NotNull final String notId = UUID.randomUUID().toString();
        Assert.assertTrue(sessionRepository.existsById(isId));
        Assert.assertFalse(sessionRepository.existsById(notId));
    }

    @Test
    public void getCountTest() {
        Assert.assertEquals(repositorySize, sessionRepository.getCount());
    }


    @Test
    public void removeByIdTest() {
        for (@NotNull final Session session : sessionList) {
            sessionRepository.removeById(session.getId());
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> sessionRepository.findById(session.getId()));
        }
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final List<Session> sessions = sessionRepository.findAllByUserId(USER_ID_1);
        Assert.assertEquals(3, sessions.size());
        Assert.assertEquals(USER_ID_1, sessions.get(0).getUserId());
    }

}
