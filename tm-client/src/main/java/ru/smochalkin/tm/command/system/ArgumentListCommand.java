package ru.smochalkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractSystemCommand;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;

import java.util.List;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String arg() {
        return "-arg";
    }

    @Override
    @NotNull
    public String name() {
        return "arguments";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final List<String> keys = serviceLocator.getCommandService().getCommandArgs();
        for (String value : keys) {
            if (value == null || value.isEmpty()) continue;
            System.out.println(value);
        }
    }

}
