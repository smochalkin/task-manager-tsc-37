package ru.smochalkin.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.smochalkin.tm.api.repository.ICommandRepository;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.constant.TerminalConst;
import ru.smochalkin.tm.endpoint.*;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.system.UnknownCommandException;
import ru.smochalkin.tm.repository.CommandRepository;
import ru.smochalkin.tm.repository.ProjectRepository;
import ru.smochalkin.tm.repository.TaskRepository;
import ru.smochalkin.tm.repository.UserRepository;
import ru.smochalkin.tm.service.*;
import ru.smochalkin.tm.util.SystemUtil;
import ru.smochalkin.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpointService().getAdminEndpointPort();

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Nullable
    private Session session;

    private void init() {
        initPID();
        initCommands();
        fileScanner.init();
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.smochalkin.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class).
                        stream().
                        sorted(Comparator.comparing(Class::getName)).
                        collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            registry(clazz.getConstructor().newInstance());
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager-client.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO CLIENT TASK MANAGER **");
        init();
        parseArgs(args);
        process();
    }

    public void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public void process() {
        logService.debug("process start...");
        @NotNull String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("Enter command: ");
            command = TerminalUtil.nextLine();
            logService.command(command);
            try {
                parseCommand(command);
                logService.info("`" + command + "` command executed");
            } catch (Exception e) {
                logService.error(e);
            }
            System.out.println();
        }
    }

    public void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException();
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
